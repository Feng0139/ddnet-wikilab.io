---
title: "Game Tiles"
explain: true
weight: 40
---

#### How the tiles look in 'entities'.

Entities allow you to see how the map you are playing looks internally, leaving out the map textures.
Bind the 'Show entities' key in the controls to toggle between textures and entities.

{{% explain file="static/explain/entities_clear.svg" %}}
