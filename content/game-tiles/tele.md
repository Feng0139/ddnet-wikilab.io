---
title: "Teleporter Layer"
explain: true
weight: 50
---

#### Tile representations of the 'tele' layer in the editor.

Teleporter tile layer of maps.
Includes all relevant tiles for teleporters.

{{% explain file="static/explain/tele.svg" %}}
